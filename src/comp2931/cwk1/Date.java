// Class for COMP2931 Coursework 1

package comp2931.cwk1;
import java.util.Calendar;

/**
 * Simple representation of a date.
 */
public class Date {

  private int year;
  private int month;
  private int day;

  /**
   * Creates a Date object representing the current date.
   */
  public Date() {
    Calendar now = Calendar.getInstance();
    year = now.get(Calendar.YEAR);
    month = now.get(Calendar.MONTH) + 1; // because indexed from 0 to 11
    day = now.get(Calendar.DAY_OF_MONTH);

    checkValidDate(year, month, day);
  }

  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    year = y;
    month = m;
    day = d;

    checkValidDate(y, m, d);
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  /**
   * Returns a value for the day of the year for this date.
   * For example December 31st will have a value of 365.
   *
   * Now also handles leap years which have 366 days.
   * Such that December 31st on a leap year will have a value of 366.
   *
   * @return Day of the year
   */
  public int getDayOfYear() {
    int dayOfYear = 0;
    int[] daysInMonths = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    if( isLeapYear(year) ) {
      daysInMonths[1] = 29;
    }

    // add days from previous months
    for( int index = 0; index < (daysInMonths.length - 1); index++ ) {
      if( (month - 1) > index) {
        dayOfYear += daysInMonths[index];
      }
      else {
        break;
      }
    }
    // add current day value
    dayOfYear += day;
    return dayOfYear;
  }

  /**
   * Tests whether a Dates year is a leap year or not.
   * @param y the Dates year to check if its a leap year or not.
   *
   * @return true if year is leap year, otherwise false
   */
  public boolean isLeapYear(int y) {
    if(y % 4 == 0 && y % 100 != 0) {
      return true;
    }
    else if(y % 100 == 0 && y % 400 == 0) {
      return true;
    }
    else {
      return false;
    }
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%02d", year, month, day);
  }

  /**
   * Tests whether this date is equal to another.
   * @param other the name of the other Date object we want to compare against
   *
   * The two objects are considered equal if both are instances of
   * the Date class and both represent exactly the same date (same year,
   * month and day)
   *
   * @return true if this Date object is equal to the other, false otherwise
   */
  @Override
  public boolean equals( Object other ) {
    if (other == this) {
      return true;
    }
    else if( !(other instanceof Date) ) {
      return false;
    }
    else {
      Date otherDate = (Date) other;
      return getDay() == otherDate.getDay()
        && getMonth() == otherDate.getMonth()
        && getYear() == otherDate.getYear();
    }
  }

  private void checkValidDate(int y, int m, int d) {

    // checking year and month range
    if(y < 1) {
      throw new IllegalArgumentException("year out of range");
    }
    if(m < 1 || m > 12) {
      throw new IllegalArgumentException("month out of range");
    }

    // if leap year
    if( isLeapYear(y) ) {
      if (m == 2 && (d < 1 || d > 29)) {
        throw new IllegalArgumentException("day out of range");
      }
    }
    else { // not a leap year
      if(m == 2 && (d < 1 || d > 28) ) {
          throw new IllegalArgumentException("day out of range");
        }
    }

    // checking day range depending on month
    if(m == 4 || m == 6 || m == 9 || m == 11) {
      if(d < 1 || d > 30) {
        throw new IllegalArgumentException("day out of range");
      }
    }
    else {
      if(d < 1 || d > 31) {
        throw new IllegalArgumentException("day out of range");
      }
    }
  }

}
