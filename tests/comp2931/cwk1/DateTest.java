package comp2931.cwk1;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class DateTest {

  private Date november4th2017;
  private Date january1st1950;
  private Date october31st2017;
  private Date december25th1985;
  private Date june15th2017;
  private Date december31st2017;

  // leap year tests
  private Date february29th2000;
  private Date december31st2012;

  @Before
  public void setUp() {
    november4th2017 = new Date(2017,11,4);
    january1st1950 = new Date(1950,1,1);
    october31st2017 = new Date(2017,10,31);
    december25th1985 = new Date(1985,12,25);
    june15th2017 = new Date(2017,6,15);
    december31st2017 = new Date(2017,12,31);
    february29th2000 = new Date(2000,2,29);
    december31st2012 = new Date(2012,12,31);
  }

  @Test
  public void dateToString() {
    assertThat(november4th2017.toString(), is("2017-11-04") );
    assertThat(january1st1950.toString(), is("1950-01-01") );
  }

  @Test
  public void days() {
    assertThat(january1st1950.getDay(), is(1) );
    assertThat(november4th2017.getDay(), is(4) );
    assertThat(october31st2017.getDay(), is(31) );
  }

  @Test(expected = IllegalArgumentException.class)
  public void daysTooLow() {
    new Date(2017,4,0);
    new Date(2017,4,-1);
  }

  @Test(expected = IllegalArgumentException.class)
  public void daysTooHigh() {
    new Date(2017,2,29);
    new Date(2017,4,31);
    new Date(2017,9,32);
  }

  @Test
  public void months() {
    assertThat(january1st1950.getMonth(), is(1) );
    assertThat(october31st2017.getMonth(), is(10) );
    assertThat(december25th1985.getMonth(), is(12) );
  }

  @Test(expected = IllegalArgumentException.class)
  public void monthTooLow() {
    new Date(2017,0,20);
    new Date(2017,-1,20);
  }

  @Test(expected = IllegalArgumentException.class)
  public void monthTooHigh() {
    new Date(2017,13,15);
  }

  @Test
  public void years() {
    assertThat(december25th1985.getYear(), is(1985) );
    assertThat(october31st2017.getYear(), is(2017) );
  }

  @Test(expected = IllegalArgumentException.class)
  public void yearTooLow() {
    new Date(0,1,1);
    new Date(-1,1,1);
  }

  @Test
  public void equality() {
    assertTrue(november4th2017.equals(november4th2017) );
    assertTrue(november4th2017.equals(new Date(2017,11,4) ) );
    assertFalse(november4th2017.equals(new Date(2017,11,5) ) );
    assertFalse(november4th2017.equals(new Date(2017,10,4) ) );
    assertFalse(november4th2017.equals(new Date(2018,11,4) ) );
  }

  @Test
  public void dayOfYear() {
    assertThat(january1st1950.getDayOfYear(), is(1) );
    assertThat(june15th2017.getDayOfYear(), is(166) );
    assertThat(december31st2017.getDayOfYear(), is(365) );
  }

  @Test
  public void isLeapYear() {
    assertTrue(february29th2000.isLeapYear(february29th2000.getYear()) );
    assertTrue(december31st2012.isLeapYear(december31st2012.getYear()) );
    assertFalse(june15th2017.isLeapYear(june15th2017.getYear()) );
  }

  @Test
  public void leapYearDayOfYear() {
    assertThat(february29th2000.getDayOfYear(), is(60) );
    assertThat(december31st2012.getDayOfYear(), is(366) );
  }

  @Test
  public void leapYearDays() {
    assertThat(february29th2000.getDay(), is(29) );
  }

  @Test(expected = IllegalArgumentException.class)
  public void leapYearDaysTooHigh() {
    new Date(2000,2,30);
    new Date(2012,2,30);
  }

}